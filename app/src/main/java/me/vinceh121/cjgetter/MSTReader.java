package me.vinceh121.cjgetter;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import xyz.bowser65.rfidutils.RfidUtils;

public class MSTReader {
    private BufferedReader read;
    private String[] files;
    private byte[][] sector0, sector1;

    public MSTReader() {
        File mctFolder = new File(Environment.getExternalStorageDirectory().getPath() + "/MifareClassicTool/dump-files/");
        System.out.println(mctFolder.getAbsolutePath());
        if (mctFolder.canRead()) {
            final File[] far = mctFolder.listFiles();

            files = new String[far.length];
            for (int i = 0; i < far.length; i++) {
                files[i] = far[i].getName();
            }
        } else {
            System.err.println("Do not have permission to read MST folder");
        }
    }

    public byte[][] getSector0() {
        return sector0;
    }

    public byte[][] getSector1() {
        return sector1;
    }

    public String[] getList() {
        return files;
    }

    public void read(String name) throws IOException {
        read = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() + "/MifareClassicTool/dump-files/" + name));
        sector0 = new byte[4][16];
        sector1 = new byte[4][16];
        int sectorRead = -1;
        int blockIndex = 0;
        String buf;

        while ((buf = read.readLine()) != null && sectorRead != -2) {
            if (buf.contains("-"))
                buf = buf.replaceAll(Pattern.quote("-"), "0"); // Replace all - by 0 in case of missing keys

            if (buf.startsWith("+")) {
                blockIndex = 0;
                if (buf.equals("+Sector: 0")) {
                    sectorRead = 0;
                } else if (buf.equals("+Sector: 1")) {
                    sectorRead = 1;
                } else {
                    sectorRead = -2;
                }
            } else {
                if (sectorRead == 0) {
                    sector0[blockIndex] = RfidUtils.hexStringToByteArray(buf);
                    blockIndex++;
                } else if (sectorRead == 1) {
                    sector1[blockIndex] = RfidUtils.hexStringToByteArray(buf);
                    blockIndex++;
                } else {
                    throw new IllegalStateException("Illegal sector number to be read: " + sectorRead);
                }
            }


        }
    }

    public void close() throws IOException {
        read.close();
    }
}
