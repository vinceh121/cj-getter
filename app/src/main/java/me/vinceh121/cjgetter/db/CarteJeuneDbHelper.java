package me.vinceh121.cjgetter.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Vector;

import me.vinceh121.cjgetter.CarteJeune;
import xyz.bowser65.rfidutils.RfidUtils;

public class CarteJeuneDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CarteJeune.db";

    public CarteJeuneDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE cards (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "sector0 BLOB," +
                "sector1 BLOB," +
                "first_name TEXT," +
                "last_name TEXT," +
                "comments TEXT," +
                "account_number TEXT," +
                "card_number TEXT," +
                "barcode_number TEXT," +
                "establishment TEXT," +
                "teacher BOOL," +
                "student BOOL," +
                "admin BOOL)");
    }

    public CarteJeune getSingleCardFromNumber(String cardNumber) {
        CarteJeune[] cards = getCardsFromNumber(cardNumber);
        if (cards.length == 0)
            return null;
        else if (cards.length == 1)
            return cards[0];
        else
            throw new IllegalArgumentException("Too manwy cawds senpai");
    }

    public CarteJeune[] getCardsFromNumber(String cardNumber) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cur = db.rawQuery("SELECT * FROM cards WHERE card_number = ?", new String[]{cardNumber});
        Vector<CarteJeune> cards = new Vector<>();
        while (cur.moveToNext()) {
            cards.add(cardFromCursor(cur));
        }
        cur.close();
        db.close();
        return cards.toArray(new CarteJeune[cards.size()]);
    }

    public void addCard(CarteJeune cj) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert("cards", null, cj.getContentValues(false));
        db.close();
    }

    public void updateCard(CarteJeune cj) {
        SQLiteDatabase db = getWritableDatabase();
        db.update("cards", cj.getContentValues(true), "id = ?", new String[]{String.valueOf(cj.getId())});
        db.close();
    }

    public void deleteCard(CarteJeune cj) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("cards", "id = ?", new String[]{String.valueOf(cj.getId())});
        db.close();
    }

    public static CarteJeune cardFromCursor(Cursor cur) {
        CarteJeune cj = new CarteJeune();
        cj.setId(cur.getInt(cur.getColumnIndexOrThrow("id")));
        cj.setSector0(RfidUtils.splitArrayIntoMatrix(cur.getBlob(cur.getColumnIndexOrThrow("sector0"))));
        cj.setSector1(RfidUtils.splitArrayIntoMatrix(cur.getBlob(cur.getColumnIndexOrThrow("sector1"))));
        cj.setFirstName(cur.getString(cur.getColumnIndexOrThrow("first_name")));
        cj.setLastName(cur.getString(cur.getColumnIndexOrThrow("last_name")));
        cj.setComments(cur.getString(cur.getColumnIndexOrThrow("comments")));
        cj.setAccountNumber(cur.getString(cur.getColumnIndexOrThrow("account_number")));
        cj.setCardNumber(cur.getString(cur.getColumnIndexOrThrow("card_number")));
        cj.setBarcodeNumber(cur.getString(cur.getColumnIndexOrThrow("barcode_number")));
        cj.setEstablishment(cur.getString(cur.getColumnIndexOrThrow("establishment")));
        cj.setTeacher(cur.getString(cur.getColumnIndexOrThrow("teacher")).equals("1"));
        cj.setStudent(cur.getString(cur.getColumnIndexOrThrow("student")).equals("1"));
        cj.setAdministration(cur.getString(cur.getColumnIndexOrThrow("admin")).equals("1"));
        return cj;
    }

    public CarteJeune[] getSavedCards(String sort) {
        if (sort == null)
            sort = "card_number";
        Cursor cur = getReadableDatabase().rawQuery("SELECT * FROM cards ORDER BY " + sort + " DESC", null);
        if (cur.getCount() == 0)
            return null;
        Vector<CarteJeune> cards = new Vector<>();
        while (cur.moveToNext()) {
            cards.add(cardFromCursor(cur));
        }
        cur.close();
        return cards.toArray(new CarteJeune[cards.size()]);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
