package me.vinceh121.cjgetter;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import com.google.android.material.snackbar.Snackbar;

import me.vinceh121.cjgetter.db.CarteJeuneDbHelper;
import me.vinceh121.cjgetter.ui.main.DataFragment;
import xyz.bowser65.rfidutils.RfidUtils;

public class CardViewActivity extends AppCompatActivity {
    private CarteJeune card;
    private DataFragment data;
    private boolean waitingWrite = false;
    private AlertDialog alertDialog;
    protected PendingIntent pendingIntent;
    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent intent = new Intent(this, this.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        card = (CarteJeune) getIntent().getSerializableExtra("carte_jeune");
        data = (DataFragment) getSupportFragmentManager().findFragmentById(R.id.data_fragment);
        data.populate(card);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.menu_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_card_save:
                saveCard();
                return true;
            case R.id.action_card_write:
                writeCard();
                return true;
            case R.id.action_card_delete:
                new AlertDialog.Builder(this).setMessage(getString(R.string.view_yeet_confirm))
                        .setNegativeButton(getString(R.string.view_yeet_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).setPositiveButton(getString(R.string.view_yeet_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteCard();
                    }
                }).show();
                return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        data.onActivityResult(requestCode, resultCode, intent);
    }

    private void deleteCard() {
        CarteJeune card = data.generateCard();
        System.out.println("Card id:" + card.getId());
        CarteJeuneDbHelper helper = new CarteJeuneDbHelper(this);
        helper.deleteCard(card);
        helper.close();
        Toast.makeText(this, getString(R.string.view_yeeted), Toast.LENGTH_LONG).show();
        NavUtils.navigateUpFromSameTask(this);
    }

    private void saveCard() {
        CarteJeune newCard = data.generateCard();
        CarteJeuneDbHelper helper = new CarteJeuneDbHelper(this);
        helper.updateCard(newCard);
        helper.close();
        Snackbar.make(data.getView(), getString(R.string.view_saved), Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            if (!waitingWrite)
                return;
            Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            MifareClassic mfc = MifareClassic.get(tagFromIntent);
            try {
                mfc.connect();
                boolean auth = RfidUtils.authenticateB(mfc, 1);
                Log.d("writeCard", String.valueOf(auth));
                if (auth) {
                    alertDialog.setMessage(getString(R.string.view_writing));
                    for (int i = 0; i < 4; i++) {
                        alertDialog.setMessage("Writing " + i + "/4");
                        Log.i("WRITE", "Writing block " + (i + 4));
                        mfc.writeBlock(i + 4, data.getCardDataSector1()[i]);
                    }
                    alertDialog.setMessage(getString(R.string.view_wrote));
                    alertDialog.setCancelable(true);
                } else {
                    alertDialog.setMessage(getString(R.string.view_auth_failed));
                    alertDialog.setCancelable(true);
                }
                mfc.close();

            } catch (Exception e) {
                e.printStackTrace();
                alertDialog.setMessage(getString(R.string.view_write_failed) + e.toString());
            }
        }
    }

    private void writeCard() {
        alertDialog = new AlertDialog.Builder(this).setMessage(getString(R.string.view_waiting_card)).show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                waitingWrite = false;
            }
        });
        this.waitingWrite = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{}, null);
    }
}
