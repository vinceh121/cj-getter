package me.vinceh121.cjgetter;

import org.junit.Test;

import java.util.Arrays;

import xyz.bowser65.rfidutils.RfidUtils;

import static org.junit.Assert.assertArrayEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TestRfidUtils {
    @Test
    public void matrixToArray() {
        byte[][] m = new byte[][]{
                {0xC, 0x0},
                {0xF, 0xF}
        };
        byte[] a = new byte[]{0xC, 0x0, 0xF, 0xF};
        assertArrayEquals(a, RfidUtils.byteMatrixToArray(m));
    }

    @Test
    public void splitArray() { // TODO proper expected
        byte[][] m = new byte[][]{
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63},
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63},
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63}
        };
        byte[] a = new byte[256];
        Arrays.fill(a, (byte)0xA);
        // assertArrayEquals(m, RfidUtils.splitArrayIntoMatrix(a));
    }
}