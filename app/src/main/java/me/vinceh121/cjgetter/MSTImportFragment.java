package me.vinceh121.cjgetter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import java.io.IOException;


public class MSTImportFragment extends Fragment {
    private RadioGroup radioGroup;
    private MSTReader read;
    private byte[][] sector0, sector1;

    public MSTImportFragment() {
    }

    public static MSTImportFragment newInstance() {
        MSTImportFragment fragment = new MSTImportFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        read = new MSTReader();
    }

    public String getSelectedDump() {
        return ((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
    }

    private void read(String name) {
        try {
            read.read(name);
            sector0 = read.getSector0();
            sector1 = read.getSector1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mctimport, container, false);
        radioGroup = v.findViewById(R.id.radio_dumps);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                read(getSelectedDump());
            }
        });

        for (String dump : read.getList()) {
            RadioButton radio = new RadioButton(getContext());
            radio.setText(dump);
            radioGroup.addView(radio);
        }
        return v;
    }

    public byte[][] getSector0() {
        return sector0;
    }

    public byte[][] getSector1() {
        return sector1;
    }
}
