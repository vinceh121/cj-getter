package me.vinceh121.cjgetter;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import me.vinceh121.cjgetter.db.CarteJeuneDbHelper;
import me.vinceh121.cjgetter.ui.main.DataFragment;
import me.vinceh121.cjgetter.ui.main.ScanFragment;
import xyz.bowser65.rfidutils.RfidUtils;

public class NewCardActivity extends AppCompatActivity {
    private NfcAdapter nfcAdapter;
    private ScanFragment scan;
    private DataFragment data;

    private FloatingActionButton fab; // Floating button to create tag in second step
    protected PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_card);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent intent = new Intent(this, this.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        scan = ScanFragment.newInstance(this);
        data = DataFragment.newInstance();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_frame, scan)
                .commit();

        fab = findViewById(R.id.fabNewCard);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(NewCardActivity.this, getString(R.string.new_saving), Toast.LENGTH_LONG);
                CarteJeuneDbHelper opener = new CarteJeuneDbHelper(NewCardActivity.this);
                opener.addCard(data.generateCard());
                opener.close();
                NavUtils.navigateUpFromSameTask(NewCardActivity.this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        data.onActivityResult(requestCode, resultCode, intent);
    }

    public void showData() {
        fab.show();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, data).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            boolean succ = scan.handleTagDiscovered(intent);
            if (succ) {
                Log.i("CJ-NFC", "Found tag with block 0 " + RfidUtils.toHex(scan.getCardDataSector0()[0]));

                CarteJeuneDbHelper helper = new CarteJeuneDbHelper(this);
                CarteJeune card = helper.getSingleCardFromNumber(RfidUtils.computeCardNumber(scan.getCardDataSector1()[0]));

                if (card == null) { // If card already exists
                    addCard();
                } else { // Card already exists, use ugly code to show confirm dialog
                    new AlertDialog.Builder(this)
                            .setTitle("Duplicate card")
                            .setMessage("This card is already in the database according to the card number. Add it anyway?")
                            .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    addCard();
                                }
                            }).setNegativeButton("fuck off you fucking dupe", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
            }
        }
    }

    private void addCard() {
        data.setCardDataSector0(scan.getCardDataSector0());
        data.setCardDataSector1(scan.getCardDataSector1());

        showData();
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{}, null);
    }
}