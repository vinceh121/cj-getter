package me.vinceh121.cjgetter.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import me.vinceh121.cjgetter.CarteJeune;
import me.vinceh121.cjgetter.R;
import xyz.bowser65.rfidutils.RfidUtils;

public class DataFragment extends Fragment {
    private byte[][] cardDataSector0;
    private byte[][] cardDataSector1;
    private EditText firstName, lastName, accNum, cardNum, barcode, establishment, editSector0,
            editSector1, id, comment;
    private ChipGroup chips;
    private Chip chipStudent, chipAdmin, chipTeacher;

    public DataFragment() {

    }

    public static DataFragment newInstance() {
        DataFragment fragment = new DataFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_data, container, false);

        ImageView btnBarcode = v.findViewById(R.id.btnBarcodeScan);
        btnBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(DataFragment.this.getActivity());
                integrator.initiateScan();
            }
        });

        editSector0 = v.findViewById(R.id.editSector0);

        editSector1 = v.findViewById(R.id.editSector1);
        updateHexDump();

        id = v.findViewById(R.id.editId);

        firstName = v.findViewById(R.id.editFirstName);
        lastName = v.findViewById(R.id.editLastName);
        accNum = v.findViewById(R.id.editAccNum);
        cardNum = v.findViewById(R.id.editCardNum);
        cardNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.charAt(0) == '?') {
                    updateCardNumber(true);
                }
            }
        });
        barcode = v.findViewById(R.id.editBarcode);
        establishment = v.findViewById(R.id.editEstablishment);
        comment = v.findViewById(R.id.editComment);

        chips = v.findViewById(R.id.chipGroup);
        chipAdmin = v.findViewById(R.id.chipAdmin);
        chipStudent = v.findViewById(R.id.chipStudent);
        chipTeacher = v.findViewById(R.id.chipTeacher);
        return v;
    }

    private void updateHexDump() {
        editSector0.setText(RfidUtils.matrixToString(cardDataSector0));
        editSector1.setText(RfidUtils.matrixToString(cardDataSector1));
    }

    private void updateCardNumber(boolean force) {
        if ((cardNum != null && cardNum.getText().toString().isEmpty() && getCardDataSector1() != null) || force) {
            cardNum.setText(RfidUtils.computeCardNumber(cardDataSector1[0]));
            final Snackbar snack = Snackbar.make(this.getView(), "gnuh", Snackbar.LENGTH_INDEFINITE);
            snack.setAction("kthxbye", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snack.dismiss();
                }
            });
            snack.show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            System.out.println("Scan result: " + scanResult.toString());
            barcode.setText(scanResult.getContents());
        } else {
            Toast.makeText(this.getContext(), "Null scan results", Toast.LENGTH_LONG).show();
        }
    }

    private void updateBlockData() {
        setCardDataSector0(RfidUtils.hexStringToByteMatrix(editSector0.getText().toString()));
        setCardDataSector1(RfidUtils.hexStringToByteMatrix(editSector1.getText().toString()));

        Log.w("CJ-NFC", "Block 0 data: " + RfidUtils.toHex(getCardDataSector0()[0]));
        id.setText(String.valueOf(RfidUtils.uidFromBlock0(getCardDataSector0()[0])));
    }


    public CarteJeune generateCard() {
        CarteJeune c = new CarteJeune();
        c.setFirstName(firstName.getText().toString());
        c.setLastName(lastName.getText().toString());
        c.setAccountNumber(accNum.getText().toString());
        c.setCardNumber(cardNum.getText().toString());
        c.setBarcodeNumber(barcode.getText().toString());
        c.setEstablishment(establishment.getText().toString());
        c.setComments(comment.getText().toString());
        try {
            c.setId(Integer.parseInt(id.getText().toString()));
        } catch (NumberFormatException e) {
            c.setId(-1);
        }

        c.setAdministration(chipAdmin.isChecked());
        c.setTeacher(chipTeacher.isChecked());
        c.setStudent(chipStudent.isChecked());

        updateBlockData();

        c.setSector0(cardDataSector0);
        c.setSector1(cardDataSector1);

        return c;
    }

    public byte[][] getCardDataSector0() {
        return cardDataSector0;
    }

    public void setCardDataSector0(byte[][] cardDataSector0) {
        this.cardDataSector0 = cardDataSector0;
    }

    public byte[][] getCardDataSector1() {
        return cardDataSector1;
    }

    public void setCardDataSector1(byte[][] cardDataSector1) {
        this.cardDataSector1 = cardDataSector1;
        updateCardNumber(false);
    }

    public void populate(CarteJeune card) {
        firstName.setText(card.getFirstName());
        lastName.setText(card.getLastName());
        accNum.setText(card.getAccountNumber());
        cardNum.setText(card.getCardNumber());
        barcode.setText(card.getBarcodeNumber());
        establishment.setText(card.getEstablishment());
        comment.setText(card.getComments());
        id.setText(String.valueOf(card.getId()));

        chipStudent.setChecked(card.isStudent());
        chipAdmin.setChecked(card.isAdministration());
        chipTeacher.setChecked(card.isTeacher());

        setCardDataSector0(card.getSector0());
        setCardDataSector1(card.getSector1()); // Set sector after card number so we don't trigger auto calculation

        updateHexDump();
    }
}
