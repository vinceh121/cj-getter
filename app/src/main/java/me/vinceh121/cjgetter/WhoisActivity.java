package me.vinceh121.cjgetter;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;

import me.vinceh121.cjgetter.db.CarteJeuneDbHelper;
import xyz.bowser65.rfidutils.RfidUtils;

public class WhoisActivity extends AppCompatActivity {
    protected PendingIntent pendingIntent;
    private NfcAdapter nfcAdapter;
    private TextView txtName;
    private TextView txtBarcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whois);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent intent = new Intent(this, this.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);


        txtName = (TextView) findViewById(R.id.whois_name);
        txtBarcode = (TextView) findViewById(R.id.whois_barcode);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            MifareClassic mfc = MifareClassic.get(tagFromIntent);
            try {
                mfc.connect();
                boolean auth = RfidUtils.authenticate(mfc, 1);
                if (auth) {
                    byte[] block = mfc.readBlock(4);
                    CarteJeuneDbHelper helper = new CarteJeuneDbHelper(this);
                    try {
                        CarteJeune cj = helper.getSingleCardFromNumber(RfidUtils.computeCardNumber(block));
                        if (cj != null) {
                            final String firstName = cj.getFirstName();
                            final String lastName = cj.getLastName();
                            final String cardNumber = cj.getCardNumber();
                            txtName.setText(firstName + " " + lastName);
                            txtBarcode.setText(cardNumber);
                        } else {
                            Snackbar.make(txtName, getString(R.string.whois_unknown), Snackbar.LENGTH_SHORT).show();
                        }
                    } catch (IllegalArgumentException e) {
                        Snackbar.make(txtName, getString(R.string.whois_too_much), Snackbar.LENGTH_LONG).show();
                    }
                    helper.close();
                } else {
                    Snackbar.make(txtName, getString(R.string.whois_auth_failed), Snackbar.LENGTH_LONG).show();
                }
                mfc.close();
            } catch (Exception e) {
                e.printStackTrace();
                Snackbar.make(txtName, e.toString(), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{}, null);
    }
}
