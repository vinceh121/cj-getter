package me.vinceh121.cjgetter;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import me.vinceh121.cjgetter.db.CarteJeuneDbHelper;
import me.vinceh121.cjgetter.ui.main.DataFragment;

public class MSTImportActivity extends AppCompatActivity {
    private static final int STATE_SELECT = 0, STATE_DATA = 1;
    private DataFragment data;
    private MSTImportFragment mctimport;

    private int state = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mct_import);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data = DataFragment.newInstance();
        mctimport = MSTImportFragment.newInstance();

        getSupportFragmentManager().beginTransaction().add(R.id.content_frame, mctimport).commit();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state == STATE_SELECT) {
                    data.setCardDataSector0(mctimport.getSector0());
                    data.setCardDataSector1(mctimport.getSector1());
                    getSupportFragmentManager().beginTransaction().detach(mctimport).add(R.id.content_frame, data).commit();
                    state = STATE_DATA;
                } else if (state == STATE_DATA) {
                    save();
                } else {
                    throw new IllegalStateException("Invalid import state: " + state);
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void save() {
        Toast.makeText(MSTImportActivity.this, getString(R.string.mst_import_saving), Toast.LENGTH_LONG).show();
        CarteJeuneDbHelper opener = new CarteJeuneDbHelper(MSTImportActivity.this);
        opener.addCard(data.generateCard());
        NavUtils.navigateUpFromSameTask(MSTImportActivity.this);
    }

}
