package me.vinceh121.cjgetter;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Vector;

import me.vinceh121.cjgetter.db.CarteJeuneDbHelper;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private String sortType = "card_number";
    private String search = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NewCardActivity.class));
            }
        });
        recyclerView = findViewById(R.id.cardList);

        // use this setting to improve performance if you know that changes
        // in content do not change the list_card_view size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear list_card_view manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        updateAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateAdapter();
    }

    private void updateAdapter() {
        final CarteJeuneDbHelper db = new CarteJeuneDbHelper(this);
        CarteJeune[] cards = db.getSavedCards(sortType);
        Vector<CarteJeune> newCards = new Vector<>();
        if (search != null) {
            for (CarteJeune card : cards) {
                if (card.getSearchableString().toLowerCase().contains(search.toLowerCase())) {
                    newCards.add(card);
                }
            }
            mAdapter = new MyAdapter(newCards.toArray(new CarteJeune[newCards.size()]));
        } else {
            mAdapter = new MyAdapter(cards);
        }

        recyclerView.setAdapter(mAdapter);

        findViewById(R.id.text_empty).setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.INVISIBLE);

        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        View searchView = menu.findItem(R.id.app_bar_search).getActionView();
        if (searchView instanceof SearchView) {
            prepareSearchView((SearchView) searchView);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.menu_whois) {
            startActivity(new Intent(this, WhoisActivity.class));
            return true;
        } else if (id == R.id.menu_mct_import) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, MSTImportActivity.class));
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        0);
            }
            return true;
        } else if (item.getGroupId() == R.id.menu_sort_group) {
            if (id == R.id.menu_sort_cardnum) {
                sortType = "card_number";
                item.setChecked(true);
            } else if (id == R.id.menu_sort_firstname) {
                sortType = "first_name";
                item.setChecked(true);
            } else if (id == R.id.menu_sort_lastname) {
                sortType = "last_name";
                item.setChecked(true);
            }
            updateAdapter();
            return true;
        } else if (id == R.id.menu_about) {
            new AlertDialog.Builder(this)
                    .setMessage(BuildConfig.APPLICATION_ID + " " + BuildConfig.FLAVOR + " " + BuildConfig.VERSION_NAME)
                    .setTitle("About")
                    .show();
            return true;
        } else if (id == R.id.menu_synth) {
            startActivity(new Intent(this, SynthActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void prepareSearchView(SearchView view) {
        view.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                search = null;
                return false;
            }
        });
        view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search = newText;
                updateAdapter();
                return false;
            }
        });
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private CarteJeune[] mDataset;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        private class MyViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public View view;

            public MyViewHolder(View v) {
                super(v);
                view = v;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(CarteJeune[] myDataset) {
            mDataset = myDataset;
        }

        // Create new views (invoked by the list_card_view manager)
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_card_view, parent, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                    CarteJeune cj = mDataset[recyclerView.getChildLayoutPosition(v)];
                    intent.putExtra("carte_jeune", cj);
                    startActivity(intent);
                }
            });

            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }


        // Replace the contents of a view (invoked by the list_card_view manager)
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            CarteJeune cj = mDataset[position];

            TextView names = holder.view.findViewById(R.id.list_name);
            TextView cardNbr = holder.view.findViewById(R.id.list_card_number);

            ImageView imgStudent = holder.view.findViewById(R.id.imgStudent);
            ImageView imgTeacher = holder.view.findViewById(R.id.imgTeacher);
            ImageView imgAdmin = holder.view.findViewById(R.id.imgAdmin);


            names.setText(cj.getFirstName() + " " + cj.getLastName());
            cardNbr.setText(cj.getCardNumber());

            imgStudent.setVisibility(cj.isStudent() ? View.VISIBLE : View.INVISIBLE);
            imgTeacher.setVisibility(cj.isTeacher() ? View.VISIBLE : View.INVISIBLE);
            imgAdmin.setVisibility(cj.isAdministration() ? View.VISIBLE : View.INVISIBLE);
        }

        // Return the size of your dataset (invoked by the list_card_view manager)
        @Override
        public int getItemCount() {
            if (mDataset == null)
                return 0;
            return mDataset.length;
        }
    }

}
