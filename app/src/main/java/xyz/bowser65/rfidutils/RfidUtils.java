package xyz.bowser65.rfidutils;

import android.nfc.tech.MifareClassic;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class RfidUtils {
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public final static byte[] CARTE_JEUNE_KEYA = {(byte) 0xE9, (byte) 0xA5, (byte) 0x53, (byte) 0x10, (byte) 0x2E, (byte) 0xA5};
    public final static byte[] CARTE_JEUNE_KEYB = {(byte) 0x1F, (byte) 0x42, (byte) 0xAB, (byte) 0x91, (byte) 0x59, (byte) 0xEE};
    public final static byte[] CARTE_JEUNE_AC = {(byte) 0x78, (byte) 0x77, (byte) 0x88, (byte) 0x00};

    public static byte[][] getKeys() {
        return new byte[][]{
                //MifareClassic.KEY_DEFAULT,
                //MifareClassic.KEY_NFC_FORUM, // NFCForum content key
                RfidUtils.CARTE_JEUNE_KEYA, // CarteJeune KeyA
                RfidUtils.CARTE_JEUNE_KEYB, // CarteJeune KeyB
                {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}, // Blank key
                MifareClassic.KEY_MIFARE_APPLICATION_DIRECTORY // NFCForum MAD key

        };
    }

    public static boolean authenticate(MifareClassic mifare, int sector) throws IOException {
        // Attempt authenticating
        boolean authenticated = false;
        for (byte[] key : RfidUtils.getKeys()) {
            if (mifare.authenticateSectorWithKeyA(sector, key)) {
                authenticated = true;
                break;
            }
        }
        return authenticated;
    }

    public static boolean authenticateB(MifareClassic mifare, int sector) throws IOException {
        // Attempt authenticating
        boolean authenticated = false;
        for (byte[] key : RfidUtils.getKeys()) {
            if (mifare.authenticateSectorWithKeyB(sector, key)) {
                authenticated = true;
                break;
            }
        }
        return authenticated;
    }

    public static String computeCardNumber(byte[] block4) {
        if (block4 == null)
            throw new IllegalArgumentException("Sector 1 isn't set");
        String str = toHex(block4).substring(3, 8);
        return "0" + Integer.toString(Integer.parseInt(str, 16));
    }

    public static String toHex(byte[] bytes) {

        char[] hexChars = new char[bytes.length * 2];
        int j = 0;
        for (byte aByte : bytes) {
            int v = aByte & 0xFF;
            hexChars[j++] = hexArray[v >>> 4];
            hexChars[j++] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String toHex(byte[] bytes, char delimiter) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();

        char[] hexChars = new char[bytes.length * 3 - 1];
        int j = 0;
        for (int i = 0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChars[j++] = hexArray[v >>> 4];
            hexChars[j++] = hexArray[v & 0x0F];
            if (i < (bytes.length - 1)) {
                hexChars[j++] = delimiter;
            }
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByteArray(String paramString) throws IllegalArgumentException {
        int j = paramString.length();

        if (j % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }

        byte[] arrayOfByte = new byte[j / 2];
        int hiNibble, loNibble;

        for (int i = 0; i < j; i += 2) {
            hiNibble = Character.digit(paramString.charAt(i), 16);
            loNibble = Character.digit(paramString.charAt(i + 1), 16);
            if (hiNibble < 0) {
                throw new IllegalArgumentException("Illegal hex digit at position " + i);
            }
            if (loNibble < 0) {
                throw new IllegalArgumentException("Illegal hex digit at position " + (i + 1));
            }
            arrayOfByte[(i / 2)] = ((byte) ((hiNibble << 4) + loNibble));
        }
        return arrayOfByte;
    }

    public static byte[] concatenateByteArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    /**
     * @param m
     * @return
     * @author vinceh121
     */
    public static byte[] byteMatrixToArray(byte[][] m) {
        byte[] o = new byte[m.length * m[0].length];
        for (int x = 0; x < m.length; x++) {
            for (int y = 0; y < m[x].length; y++) {
                o[x * m[x].length + y] = m[x][y];
            }
        }
        return o;
    }

    /**
     * @param a
     * @return
     * @author vinceh121
     */
    public static byte[][] splitArrayIntoMatrix(byte[] a) {
        byte[][] o = new byte[4][16];
        for (int i = 0; i < 4; i++) {
            o[i] = Arrays.copyOfRange(a, i * 16, i * 16 + 16);
        }
        return o;
    }

    /**
     * @param s
     * @return
     * @author vinceh121
     */
    public static byte[][] hexStringToByteMatrix(String s) {
        String[] parts = s.split(Pattern.quote("\n"));
        byte[][] out = new byte[parts.length][];
        for (int i = 0; i < parts.length; i++) {
            out[i] = hexStringToByteArray(parts[i]);
        }
        return out;
    }

    public static String matrixToString(byte[][] m) {
        if (m == null)
            return "";
        StringBuilder sb = new StringBuilder();
        for (byte[] a : m) {
            sb.append(toHex(a));
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void populateBlockWithKeys(byte[] block, byte[] keyA, byte[] keyB) {
        if (keyA != null && keyA.length != 6)
            throw new IllegalArgumentException("keyA.length != 6");
        if (keyB != null && keyB.length != 6)
            throw new IllegalArgumentException("keyB.length != 6");

        for (int i = 0; i < 6; i++) {
            if (keyA != null)
                block[i] = keyA[i];
            if (keyB != null)
                block[i + 10] = keyB[i]; // Offset: 6 bytes keyA + 4 bytes AC
        }
    }

    public static String decToHex(String dec) {
        return Integer.toHexString(Integer.parseInt(dec));
    }

    public static byte[] generateCarteJeuneBlockFromCardNumberThisMethodNameIsntTooLongReallyItsNormalForAnUtilClass(String cardNumber) {
        final String strBlock = "000" + decToHex(cardNumber) + "000000000000000000000000";
        return hexStringToByteArray(strBlock);
    }

    public static byte[][] generateCarteJeuneSectorFromNumber(String cardNumber) {
        return new byte[][]{
                generateCarteJeuneBlockFromCardNumberThisMethodNameIsntTooLongReallyItsNormalForAnUtilClass(cardNumber),
                new byte[16],
                new byte[16],
                concatenateByteArrays(CARTE_JEUNE_KEYA, concatenateByteArrays(CARTE_JEUNE_AC, CARTE_JEUNE_KEYB))};
    }

    public static int uidFromBlock0(byte[] b) {
        if (b == null)
            return 0;
        return (b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3];
    }
}