package me.vinceh121.cjgetter;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;

import xyz.bowser65.rfidutils.RfidUtils;

public class SynthActivity extends AppCompatActivity {
    private TextView editSynthCardNum, textHexdump;
    private boolean waitingWrite = false;
    private AlertDialog alertDialog;
    private NfcAdapter nfcAdapter;
    protected PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synth);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent intent = new Intent(this, this.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        textHexdump = findViewById(R.id.textSynthDump);

        editSynthCardNum = findViewById(R.id.editSynthCardNum);
        editSynthCardNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() != 7) {
                    editSynthCardNum.setError("Card number length must be 7");
                } else {
                    editSynthCardNum.setError(null);
                    updateHexdump();
                }
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeCard();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateHexdump() {
        try {
            textHexdump.setText(
                    RfidUtils.matrixToString(
                            RfidUtils.generateCarteJeuneSectorFromNumber(
                                    editSynthCardNum.getText().toString())));
        } catch (Exception e) {
            e.printStackTrace();
            textHexdump.setText("Failed to make hexdump: " + e.toString() + "\nCheck your number starts with 0");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            if (!waitingWrite)
                return;
            Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            MifareClassic mfc = MifareClassic.get(tagFromIntent);
            try {
                mfc.connect();
                boolean auth = RfidUtils.authenticateB(mfc, 1);
                Log.d("writeCard", String.valueOf(auth));
                if (auth) {
                    alertDialog.setMessage(getString(R.string.view_writing));
                    String number = editSynthCardNum.getText().toString();
                   /* if (number.charAt(0) == '0') {
                        number = number.substring(1);
                    }*/
                    final byte[][] data = RfidUtils.generateCarteJeuneSectorFromNumber(number);
                    System.out.println("Synth data: " + Arrays.deepToString(data));
                    for (int i = 0; i < 4; i++) {
                        try {
                            alertDialog.setMessage("Writing " + i + "/4");
                            Log.i("WRITE", "Writing block " + (i + 4));
                            mfc.writeBlock(i + 4, data[i]);
                        } catch (Exception e) {
                            e.printStackTrace();
                            alertDialog.setMessage("Failed to write block " + i + ": " + e.toString());
                            break;
                        }
                    }
                    alertDialog.setMessage(getString(R.string.view_wrote));
                    alertDialog.setCancelable(true);
                } else {
                    alertDialog.setMessage(getString(R.string.view_auth_failed));
                    alertDialog.setCancelable(true);
                }
                mfc.close();
            } catch (Exception e) {
                e.printStackTrace();
                alertDialog.setMessage(getString(R.string.view_write_failed) + e.toString());
            }
        }
    }

    private void writeCard() {
        alertDialog = new AlertDialog.Builder(this).setMessage(getString(R.string.view_waiting_card)).show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                waitingWrite = false;
            }
        });
        this.waitingWrite = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{}, null);
    }

}
