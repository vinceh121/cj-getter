package me.vinceh121.cjgetter;

import android.content.ContentValues;

import java.io.Serializable;

import xyz.bowser65.rfidutils.RfidUtils;

public class CarteJeune implements Serializable {
    private int id;
    private byte[][] sector0;
    private byte[][] sector1;
    private String firstName, lastName, comments, accountNumber, cardNumber, barcodeNumber, establishment;
    private boolean isTeacher, isStudent, isAdministration;


    public ContentValues getContentValues(boolean haveId) {
        ContentValues cv = new ContentValues();
        if (haveId)
            cv.put("id", id);
        cv.put("sector0", RfidUtils.byteMatrixToArray(getSector0()));
        cv.put("sector1", RfidUtils.byteMatrixToArray(getSector1()));
        cv.put("first_name", getFirstName());
        cv.put("last_name", getLastName());
        cv.put("comments", getComments());
        cv.put("account_number", getAccountNumber());
        cv.put("card_number", getCardNumber());
        cv.put("barcode_number", getBarcodeNumber());
        cv.put("establishment", getEstablishment());
        cv.put("teacher", isTeacher());
        cv.put("student", isStudent());
        cv.put("admin", isAdministration());
        System.out.println(cv.toString());
        return cv;
    }

    @Deprecated
    public int getUid() {
        return RfidUtils.uidFromBlock0(getSector0()[0]);
    }

    public byte[][] getSector0() {
        return sector0;
    }

    public void setSector0(byte[][] sector0) {
        this.sector0 = sector0;
    }

    public byte[][] getSector1() {
        return sector1;
    }

    public void setSector1(byte[][] sector1) {
        this.sector1 = sector1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * The account number is the number that is preceeded by 'N°'
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * The account number is the number that is preceeded by 'N°'
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * The card number is the number under the barcode
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * The card number is the number under the barcode
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Number of the barcode
     */
    public String getBarcodeNumber() {
        return barcodeNumber;
    }

    /**
     * Number of the barcode
     */
    public void setBarcodeNumber(String barcodeNumber) {
        this.barcodeNumber = barcodeNumber;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public void setStudent(boolean student) {
        isStudent = student;
    }

    public boolean isAdministration() {
        return isAdministration;
    }

    public void setAdministration(boolean administration) {
        isAdministration = administration;
    }

    public String getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSearchableString() {
        return getFirstName() + " "
                + getLastName() + " "
                + getBarcodeNumber() + " "
                + getEstablishment() + " "
                + getAccountNumber() + " "
                + getCardNumber() + " "
                + getComments() + " "
                + (isAdministration() ? "Administration" : "") + " "
                + (isTeacher() ? "Teacher" : "") + " "
                + (isStudent() ? "Student" : "");
    }
}
