package me.vinceh121.cjgetter.ui.main;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.io.IOException;

import me.vinceh121.cjgetter.NewCardActivity;
import me.vinceh121.cjgetter.R;
import xyz.bowser65.rfidutils.RfidUtils;

/**
 * Most of this class was copy/pasted from Bowser65's code
 */
public class ScanFragment extends Fragment {
    private ImageView scanImage;
    private TextView textView;
    private Button btnNoNfc;
    private byte[][] cardDataSector0;
    private byte[][] cardDataSector1;
    private NewCardActivity newCardActivity;

    public static ScanFragment newInstance(@Nullable NewCardActivity newCardActivity) {
        ScanFragment fragment = new ScanFragment();
        fragment.setNewCardActivity(newCardActivity);
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_scan, container, false);
        scanImage = root.findViewById(R.id.scanImage);
        textView = root.findViewById(R.id.textView);
        btnNoNfc = root.findViewById(R.id.btnNoNfc);
        btnNoNfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newCardActivity != null)
                    newCardActivity.showData();
            }
        });
        if (newCardActivity == null)
            btnNoNfc.setVisibility(View.GONE);
        return root;
    }

    public boolean handleTagDiscovered(final Intent intent) { // Fuck async
        scanImage.setVisibility(View.VISIBLE);

        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        MifareClassic mifare = MifareClassic.get(tag);
        try {
            mifare.connect();

            /*byte[][][] data = new byte[mifare.getSectorCount()][][];
            for (int i = 0; i < mifare.getSectorCount(); i++) {
                data[i] = readSector(mifare, i);
            }*/
            cardDataSector0 = readSector(mifare, 0);
            cardDataSector1 = readSector(mifare, 1);
            RfidUtils.populateBlockWithKeys(cardDataSector1[3], RfidUtils.CARTE_JEUNE_KEYA, RfidUtils.CARTE_JEUNE_KEYB);
            mifare.close();

        } catch (TagLostException e) {
            textView.setText(getString(R.string.scan_lost));
            return false;
        } catch (Exception e) {
            textView.setText(e.getMessage());
            e.printStackTrace();
            return false;
        }
        textView.setText(getString(R.string.scan_succ));
        return true;
    }

    private byte[][] readSector(MifareClassic mifare, int sector) throws IOException {
        // Attempt authenticating
        if (!RfidUtils.authenticate(mifare, sector)) {
            new AlertDialog.Builder(this.getContext()).setMessage(getString(R.string.scan_auth_failed) + sector).create().show();
            return null;
        }

        // Read sector
        byte[][] data = new byte[mifare.getBlockCountInSector(sector)][];
        int firstBlock = mifare.sectorToBlock(sector);
        for (int i = 0; i < mifare.getBlockCountInSector(sector); i++) {
            data[i] = mifare.readBlock(firstBlock + i);
        }
        return data;
    }

    public byte[][] getCardDataSector1() {
        return cardDataSector1;
    }

    public byte[][] getCardDataSector0() {
        return cardDataSector0;
    }

    public NewCardActivity getNewCardActivity() {
        return newCardActivity;
    }

    public void setNewCardActivity(NewCardActivity newCardActivity) {
        this.newCardActivity = newCardActivity;
    }
}